from sklearn import datasets
from sklearn.cluster import KMeans
import numpy as np
import matplotlib.pyplot as plt

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                                    centers=4,
                                    cluster_std=[1.0, 2.5, 0.5, 3.0],
                                    random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

X=generate_data(500,1)
model = KMeans(n_clusters=3)
model.fit(X)
b=model.predict(X)
print(b)
plt.figure(1)
plt.scatter (X[:,0],X[:,1],c=b)
plt.scatter(model.cluster_centers_[:,0],model.cluster_centers_[:,1],marker="X")
plt.show()

#zad2
inertia_list = []
for num_clusters in range(1, 20):
    kmeans_model = KMeans(n_clusters=num_clusters, init="k-means++")
    kmeans_model.fit(X)
    inertia_list.append(kmeans_model.inertia_)
    

plt.plot(range(1,20),inertia_list)
plt.scatter(range(1,20),inertia_list)
plt.scatter(3, inertia_list[3], marker="X", s=30, c="r")
plt.xlabel("Broj klastera", size=13)
plt.ylabel("Vrijednost inercije", size=13)
plt.show()
