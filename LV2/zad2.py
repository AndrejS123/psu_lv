import numpy as np
import matplotlib.pyplot as plt

a=[]

for i in range(100):
    a.append(np.random.randint(1,7))
labels, counts = np.unique(a, return_counts=True)
plt.bar(labels, counts, align='center')
plt.hist(a,bins=60,rwidth=0.4)
print(a)
plt.show()
