import numpy as np
import matplotlib.pyplot as plt
data = np.loadtxt(open("mtcars.csv", "rb"), usecols=(1,2,3,4,5,6),delimiter=",", skiprows=1)

mpg=data[:,0]
hp=data[:,3]
cyl=data[:,1]
plt.scatter(mpg,hp,s=data[:,5]*100)
plt.xlabel("mpg")
plt.ylabel("hp")
print(mpg.min())
print(mpg.max())
print(mpg.mean())
if cyl==6:
    print(cyl.min())
    print(cyl.max())
    print(cyl.mean())

plt.show()

