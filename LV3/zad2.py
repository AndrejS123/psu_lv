import pandas as pd
import numpy as np 
import matplotlib.pyplot as plt

mtcars = pd.read_csv('mtcars.csv')
#new_mtcars=mtcars.groupby('cyl').mpg.mean().plot.bar()
#new_mtcars=mtcars.boxplot(by='cyl', column=['wt'])
#new_mtcars=mtcars.boxplot(by='am', column =['mpg'])
ax = mtcars[mtcars.am==0].plot.scatter(x='qsec',y='hp',color='Blue',label='0')
mtcars[mtcars.am==1].plot.scatter(x='qsec',y='hp',color='red',label='1',ax=ax)
plt.show()


